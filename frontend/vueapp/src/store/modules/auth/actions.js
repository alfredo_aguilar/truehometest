/* ============
 * Actions for the auth module
 * ============
 *
 * The actions that are available on the
 * auth module.
 */

import * as types from './mutation-types'
import Vue from 'vue'

export const check = ({ commit }) => {
  commit(types.CHECK)
}

export const login = ({ commit, dispatch }, token) => {
    commit(types.LOGIN, token)
}

export const logout = ({ commit, dispatch }) => {
  commit(types.LOGOUT)
  return dispatch('account/store', {}, {root: true})
}

export default {
  check,
  login,
  logout,
}