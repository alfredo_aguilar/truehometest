/* ============
 * Mutations for the auth module
 * ============
 *
 * The mutations that are available on the
 * account module.
 */

import Vue from 'vue'
import Axios from 'axios'

import {
  CHECK,
  LOGIN,
  LOGOUT,
} from './mutation-types'

export default {
  [CHECK](state) {
    const accessToken = localStorage.getItem('accessToken')
    state.authenticated = !!accessToken
    if (state.authenticated) {
      state.accessToken = accessToken
      Vue.$API.defaults.headers.common.Authorization = `JWT ${accessToken}`
    }
  },

  [LOGIN](state, token) {
    state.authenticated = true
    state.accessToken = token
    localStorage.setItem('accessToken', token)
    Vue.$API.defaults.headers.common.Authorization = `JWT ${token}`
  },

  [LOGOUT](state) {
    state.authenticated = false
    state.accessToken = ''
    localStorage.removeItem('accessToken')
    Vue.$API.defaults.headers.common.Authorization = ''
  },
}
