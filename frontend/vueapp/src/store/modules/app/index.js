
const state = {
  loadingPage: false,
  loadingContent: 0,
  pageTitle: null,
}

const mutations = {
  SET_LOADING_PAGE(state, value=false) {
    state.loadingPage = value
  },
  SET_LOADING_CONTENT(state, value=false) {
    state.loadingContent = value
  },
  INC_LOADING_CONTENT(state, value=1) {
    state.loadingContent = state.loadingContent + value
  },
  SET_PAGE_TITLE(state, value) {
    state.pageTitle = value
  },
}

const getters = {
  contentLoading: (state) => state.loadingContent,
  pageLoading: (state) => state.loadingPage,
  pageTitle: (state) => state.pageTitle,
}

const actions = {
  setLoadingPage: {
    root: true,
    handler({ commit }, value=false) {
      commit('SET_LOADING_PAGE', value)
    }
  },
  setLoadingContent: {
    root: true,
    handler({ commit }, value=false) {
      commit('SET_LOADING_CONTENT', value)
    }
  },
  incLoadingContent: {
    root: true,
    handler({ commit }, value) {
      commit('INC_LOADING_CONTENT', value)
    },
  },
  setAppPageTitle: {
    handler({ commit }, value) {
      commit('SET_PAGE_TITLE', value)
    },
  }
}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions,
}