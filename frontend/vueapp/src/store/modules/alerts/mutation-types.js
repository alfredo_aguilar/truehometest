/* ============
 * Mutation types for the account module
 * ============
 *
 * The mutation types that are available
 * on the auth module.
 */

export const STORE = 'STORE'
export const ALERT = 'ALERT'
export const ALERTS = 'ALERTS'
export const DELETE = 'DELETE'

export default {
  STORE,
  ALERT,
  ALERTS,
  DELETE,
}