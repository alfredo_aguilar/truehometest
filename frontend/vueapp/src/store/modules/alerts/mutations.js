/* ============
 * Mutations for the auth module
 * ============
 *
 * The mutations that are available on the
 * account module.
 */

import {
  STORE,
  ALERT,
  ALERTS,
  DELETE,
} from './mutation-types'

export default {
  [STORE](state, alerts) {
    state.alerts = alerts
  },
  [ALERT](state, alert) {
    state.alerts.push(alert)
    // state.alert = alert
  },
  [ALERTS](state, alerts) {
    state.alerts = alerts
  },
  [DELETE](state, alert){
    let catList = state.alerts

    if(!catList) {
      logError(type)
      return
    }

    let index = catList.indexOf(alert)
    catList.splice(index, 1)
  },
}