/* ============
 * Actions for the auth module
 * ============
 *
 * The actions that are available on the
 * auth module.
 */

import * as types from './mutation-types'
import Vue from 'vue'

export const store = ({ commit }, alerts) => {
  commit(types.STORE, alerts)
}

export const ReciveAlert = ({ commit }, alert) => {
  commit(types.ALERT, alert)
}

export const reciveAlerts = ({ commit }, alerts) => {
  commit(types.ALERTS, alerts)
}
export const removeAlert = ({ commit }, alert) => {
  commit(types.DELETE, alert)
}


export default {
  store,
  ReciveAlert,
  reciveAlerts,
  removeAlert,
}