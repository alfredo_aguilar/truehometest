/* ============
 * Mutations for the account module
 * ============
 *
 * The mutations that are available on the
 * account module.
 */

import { STORE, STORE_PERMISSIONS, UPDATE } from './mutation-types'

export default {
  [STORE](state, account) {
    state.userId = account.userId
    state.username = account.username
    state.firstName = account.firstName
    state.lastName = account.lastName
    state.secondLastName = account.secondLastName
    state.email = account.email
    // Profile information
    state.profileId = account.profileId
    state.avatar = account.avatar
    state.phone = account.phone
    state.phoneExt = account.phoneExt
    state.cellphone = account.cellphone
    state.alternativeEmail = account.alternativeEmail
    // Groups and permissions
    state.groups = account.groups
    state.groupPermissions = { ...account.permissions }
  },

  // Custom permissions (Deprecated)
  [STORE_PERMISSIONS](state, accountPermissions) {
    state.permissions = accountPermissions
  },

  [UPDATE](state, account) {
    state.address = account.address
    state.userId = account.userId
    state.username = account.username
    state.treatment = account.treatment
    state.firstName = account.firstName
    state.lastName = account.lastName
    state.secondLastName = account.secondLastName
    state.email = account.email
    // Profile information
    state.profileId = account.profileId
    state.avatar = account.avatar
    state.phone = account.phone
    state.phoneExt = account.phoneExt
    state.cellphone = account.cellphone
    state.alternativeEmail = account.alternativeEmail
    // Groups and permissions
    // state.userArea = account.userArea
    // state.userRole = account.userRole
    // state.userRoleCode = account.userRoleCode
    // state.userType = account.userType
    // state.groups = account.groups

  }
}
