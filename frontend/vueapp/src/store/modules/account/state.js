/* ============
 * State of the account module
 * ============
 *
 * The initial state of the account module.
 */

export default {
  userId: null,
  username: null,
  treatment: null,
  firstName: null,
  lastName: null,
  secondLastName: null,
  email: null,
  // Profile information
  profileId: null,
  avatar: null,
  phone: null,
  phoneExt: null,
  cellphone: null,
  alternativeEmail: null,
  address: null,
  // Groups and permissions
  userArea: null,
  userRole: null,
  userRoleCode: null,
  userType: null,
  groups: [],
  // house made permission (depracated)
  permissions: null,
  // modern django permissions
  groupPermissions: {}
}
