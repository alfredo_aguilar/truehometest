/* ============
 * Getters for the account module
 * ============
 *
 * The getters that are available on the
 * account module.
 */

import { get } from 'lodash'

const DEPRECATED_MESSAGE = (message, opts) => {
  const { shoulUse } = opts || {}
  console.groupCollapsed(`[DEPRECATED ]${message}`)
  console.trace()
  console.groupEnd()
  if (shoulUse) {
    console.warn(`YOU SHOULD USE ${ shoulUse }`)
  }
}
export default {
  userRoleIs: (state) => (...roles) => {
    DEPRECATED_MESSAGE(`ROLE CHECK (${roles.join(", ")})`)
    const userRoleCode = state.userRoleCode
    const userRole = state.userRole
    return roles.find(role => 
      userRoleCode === role || userRole === role)
  },

  userRole: (state) => state.userRoleCode,

  userRoleName: (state) => state.userRole,

  userId: (state) => state.userId,

  userIsAdmin: (state) => {
    DEPRECATED_MESSAGE('ADMIN CHECK', {
      shoulUse: 'userAccountIsAdmin'
    })
    return state.userRoleCode === 'ADM'
  },

  userCan: ({ permissions }) => (permission, resource) => {
    DEPRECATED_MESSAGE(`CLASSIC PERMISSION CHECK (${permission}:${resource})`, {
      shoulUse: 'userAccountCan'
    })
    const { more, canMatches } = permissions
    const firstK = resource !== true && resource ? resource : permission
    const keyFilter = ({ permission: p, verb: v }) => (p === firstK || v === firstK)
    let permissionList = (
      permissions[firstK] || more.filter(keyFilter)
    )

    if (!permissionList.length) {
      permissionList = canMatches.filter(keyFilter) 
    }

    if (resource === true) {
      console.log("checking simple permission", { permissionList, firstK, permission, resource, more, canMatches })
      return permissionList.length > 0
    } else if (!resource) {
      return permissionList
    } else if (!permissionList) {
      console.warn("Not access defined for ",  { permission, resource, permissions, more, canMatches })
    } else {
      const passes =  permissionList.some(({ verb, permission: rawPermission }) => verb.toLowerCase() === permission.toLowerCase())
      return passes
    }
  },
    
  /**
   *
   Modern Django Permissions
   */
  modelPermission: (state) => (app, model) => {
    const appD = state.groupPermissions[app] || {}
    return appD[model] || {}
  },

  /**
   * Check if userAccount belong to a specific group
   */
  userAccountInGroup: (state) => (groupName) =>
    (state.groups|| []).some(g => g.name === groupName),

  /**
   * Check if userAccount belongs to the Admin group
   */
  userAccountIsAdmin: (state) => state.groups.some(g => g.name === 'Admin'),

  /**
   * Checks if userAccount has a given permission in an app/model
   */
  userAccountCan: (state, getters) => (permission, app, model) => {
    return getters.modelPermission(app, model)[permission] || false
  },

  /**
   * Shortcut for userAccountCan('add')
   */
  userAccountCanAdd: (state, getters) =>
    (app, model) => getters.userAccountCan('add', app, model),
  /**
   * Shortcut for userAccountCan('view')
   */
  userAccountCanView: (state, getters) =>
    (app, model) => getters.userAccountCan('view', app, model),
  /**
   * Shortcut for userAccountCan('change')
   */
  userAccountCanChange: (state, getters) =>
    (app, model) => getters.userAccountCan('change', app, model),
  /**
   * Shortcut for userAccountCan('delete')
   */
  userAccountCanDelete: (state, getters) =>
    (app, model) => getters.userAccountCan('delete', app, model)
}
