/* ============
 * Mutation types for the account module
 * ============
 *
 * The mutation types that are available
 * on the account module.
 */

export const STORE = 'STORE'
export const STORE_PERMISSIONS = 'STORE_PERMISSIONS'
export const UPDATE = 'UPDATE'

export default {
  STORE_PERMISSIONS,
  UPDATE,
}