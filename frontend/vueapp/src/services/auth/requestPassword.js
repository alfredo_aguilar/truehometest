import Axios from 'axios'
import Vue from 'vue'

// When the request succeeds
const success = () => {
  Vue.router.push({
    name: 'login.index',
  })
}

// When the request fails
const failed = () => {
}

export default (user) => {

  Axios.post('/account/password/reset/', user)
      .then(() => {
        success()
      }).catch((error) => {
        failed(error)
      })
}