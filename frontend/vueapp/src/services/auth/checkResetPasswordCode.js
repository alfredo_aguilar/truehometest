import Axios from 'axios'
import Vue from 'vue'

// When the request succeeds
const success = () => {
}

// When the request fails
const failed = () => {
  Vue.router.push({
    name: 'register.index',
  })
}

export default (code) => {

  Axios.get('account/password/reset/verify/', { params: { code } })
      .then(success)
      .catch(failed)
}