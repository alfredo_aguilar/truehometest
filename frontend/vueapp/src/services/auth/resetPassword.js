import Vue from 'vue'
import Axios from 'axios'

// When the request succeeds
const success = () => {
  Vue.router.push({
    name: 'login.index',
  })
}

// When the request fails
const failed = () => {
}

export default (user) => {

  Axios.post('/account/password/reset/verified/', user)
      .then(() => {
        success()
      }).catch((error) => {
        failed(error)
      })
}