import Vue from 'vue'
import Axios from 'axios'
import store from './../../store'

export default () => {
  const token = store.state.auth.accessToken
  return Axios.post('auth/refresh_token/', {token})
    .then((response) => {
      const token = response.data.token
      store.dispatch('auth/login', token)
      return token
    })
}