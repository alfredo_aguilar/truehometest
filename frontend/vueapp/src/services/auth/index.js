import login from './login'
import apiLogout from './apiLogout'
import logout from './logout'
import register from './register'
import requestPassword from './requestPassword'
import resetPassword from './resetPassword'
import verifyEmail from './verifyEmail'
import refreshToken from './refreshToken'
import verifyToken from './verifyToken'
import checkResetPasswordCode from './checkResetPasswordCode'

export default {
  login,
  apiLogout,
  logout,
  register,
  requestPassword,
  resetPassword,
  verifyEmail,
  refreshToken,
  verifyToken,
  checkResetPasswordCode,
}
