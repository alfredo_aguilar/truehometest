
import Vue from 'vue'
import Axios from 'axios'
import store from './../../store'
import logout from './logout'

export default (loginParams) => 
  Axios.post('/auth/logoutall/')
    .finally(() => {
      logout()
    })
