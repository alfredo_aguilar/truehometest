import Axios from 'axios'
import Vue from 'vue'
import store from './../../store'

const returnToIndex = () => {
  store.dispatch('auth/logout')
  Vue.router.push({
    name: 'login.index',
  })
}

export default () => {
  const token = store.state.auth.accessToken
  console.log("verifyng token", token)
  const request = Axios.post('auth/token-verify/', {token})
  return request
}