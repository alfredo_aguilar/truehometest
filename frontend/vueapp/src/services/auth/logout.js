import Vue from 'vue'
import store from './../../store'

export default (loginParams) => {
  console.trace('LOGGEd OUT')
  return store.dispatch('auth/logout')
    .then((response) => {
      Vue.router.push({
        name: 'Login',
        query: loginParams
      })
      return response
    })
}
