import Vue from 'vue'
import Axios from 'axios'

// When the request succeeds
const success = () => {
  Vue.router.push({
    name: 'login.index',
  })
}

// When the request fails
const failed = () => {
}

export default (code) => {

  Axios.get('account/signup/verify/', { params: { code } })
      .then(success)
      .catch(failed)
}