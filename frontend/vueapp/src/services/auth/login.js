import Vue from 'vue'
import store from './../../store'

const login = ({ username, password }) => {
  return Vue.$API.post('/auth/obtain_token/', { username, password })
    .then(response => {
      const token = response.data.token
      store.dispatch('auth/login', token)
      return token
    })
}

export default login
