
const fetchGeolocation = () =>
  fetch('https://location.services.mozilla.com/v1/geolocate?key=test')
    .then(el => el.json())
    .catch(err => {
      throw err
    })

export default fetchGeolocation
