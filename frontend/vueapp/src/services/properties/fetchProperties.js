import Vue from 'vue'
import propertyTransformer from '../../transformers/property/property'

/**
 * @author Alfredo Ricardo Aguilar Hernandez <aguilarhernandeza@gmail.com>
 *
 *
 * Fetch activities
 * @return {Array}
 */

 const fetchProperties = () =>
  Vue.$API.get('activities/properties/')
   .then(response => {
     return propertyTransformer.fetchCollection(response.data)
   })
   .catch(err => { throw err })

export default fetchProperties