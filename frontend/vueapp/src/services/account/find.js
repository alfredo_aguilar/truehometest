import accountTransformer from './../../transformers/account'
import accountPermissionsTransformer from './../../transformers/permissions'
import store from './../../store'
import Vue from 'vue'
import Axios from 'axios'

// When the request succeeds
const success = (account, permissions) => {
  store.dispatch('account/store', accountTransformer.fetch(account))
}

export default () =>
  Axios.get('/user/current/permissions/')
    .then(({ data }) => accountPermissionsTransformer.fetch(data))
    .then(permissions => {
      console.log("SAVING", { permissions })
      store.dispatch('account/storePermissions', permissions)
      return Axios.get('/user/current/')
        .then((response) => {
          success(response.data)
        })
    })
