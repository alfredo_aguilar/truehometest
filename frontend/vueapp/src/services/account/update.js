import Vue from 'vue'
import Axios from 'axios'
import store from './../../store'
import accountTransformer from '../../transformers/account'

const updateStore = (accountData) => {
  store.dispatch('account/update', accountData)
}

export default (accountData) => {
  const data = accountTransformer.update(accountData)
  console.log(data)
  return Axios.put('/user/current/', data).then(() => updateStore(accountData))
}