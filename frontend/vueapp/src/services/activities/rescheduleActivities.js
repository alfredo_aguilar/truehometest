import Vue from 'vue'
import activityTransformer from '../../transformers/activities/activities'


export default (activity_id, new_date) => {
  let date = new_date ? new_date : ""
  console.log(activity_id)
  console.log(date)
  return Vue.$API.put(`activities/re-schedule/${activity_id}/`, activityTransformer.update(date))
    .then(response => activityTransformer.fetch(response.data))
    .catch(err => {
      throw err
    })
}
