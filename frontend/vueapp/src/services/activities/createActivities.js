import Vue from 'vue'
import activityTransformer from '../../transformers/activities/activities'

const createActivity = (payload) =>
  Vue.$API.post(`activities/`, activityTransformer.send(payload))
    .then((res) => res.data).catch((error) => {
      failure(error)
      throw error
    })

export default createActivity
