import Vue from 'vue'
import activityTransformer from '../../transformers/activities/activities'

/**
 * @author Alfredo Ricardo Aguilar Hernandez <aguilarhernandeza@gmail.com>
 *
 *
 * Fetch activities
 * @return {Array}
 */

 const fetchActivities = () =>
  Vue.$API.get('activities/')
   .then(response => {
     return activityTransformer.fetchCollection(response.data)
   })
   .catch(err => { throw err })

export default fetchActivities