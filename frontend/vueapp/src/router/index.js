// router elements
import Vue from 'vue';
import Router from 'vue-router';
import VuexRouterSync from 'vuex-router-sync'
import routes from './routes'
import store from '@/store'

Vue.use(Router);

export default new Router({
  routes: routes
});
