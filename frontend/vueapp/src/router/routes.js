import Dashboard from '@/pages/Dashboard.vue';
import Login from '@/pages/core/Login.vue';
import Error from '@/pages/core/Error.vue';
import Profile from '@/pages/profile/profile.vue'

export default [
    {
        path: '/',
        name: 'Login',
        component: Login,
        meta: {
            allowAnonymous: true
        }
    },
    {
        path: '/dashboard',
        name: 'Dashboard',
        component: Dashboard,
        meta: {
            breadcrumb: [
            { name: 'dashboard' }
            ]
        }
    },
    {
        path: '/profile',
        name: 'Profile',
        component: Profile,
        meta: {
            breadcrumb: [
            { name: 'dashboard' }
            ]
        }
    },
    {
        path: '/error',
        name: 'Error',
        component: Error,
        meta: {
            allowAnonymous: true
        }
    }
  ]
  