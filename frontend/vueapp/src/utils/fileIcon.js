

const IMAGE_ICON = {
	icon: 'image',
	type: 'md-icon'
	}
const PDF_ICON = {
	icon: 'picture_as_pdf',
	type: 'md-icon'
}

const XLS_ICON = {
	icon: 'insert_chart',
	type: 'md-icon'
}

// const XLS_ICON = {
// 	icon: 'fa fa-file-excel',
// 	type: 'fontawesome',
// }

const fileExtensionMap = {
	jpg: IMAGE_ICON,
	png: IMAGE_ICON,
	pdf: PDF_ICON,
	xls: XLS_ICON,
	xlsx: XLS_ICON,
}

export default (link) => {
	const ext = (link || "").split('.').pop()
	return fileExtensionMap[ext]
}