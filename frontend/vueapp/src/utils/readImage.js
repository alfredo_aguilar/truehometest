export default (imageFile) => {
	return new Promise(function(resolve, reject) {
		let reader = new FileReader()
		reader.onload = e => resolve(e.target.result)	
		reader.onerror = err => reject(err)

    reader.readAsDataURL(imageFile)
	})
}