export default (url) => {
  const AL = process.env.ASSETS_LOCATION
  const alreadyHave = !!url && url.includes('http://localhost:8000/')
  console.log('LOCATION', AL)
  return (AL && url && !alreadyHave) ? (AL + url) : url
}