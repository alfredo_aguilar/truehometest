
export default (object, properties) => {
  let arr = {...properties}
  let obj = {}
  for (const prop of properties) {
    obj[`${object}.${prop}`] = function()  {
      console.log(`${prop} was touched`)
      this.touched[prop] = true
    }
  }
  return obj
}
