export default (obj) => {
  const formData = new FormData()
  for (const key in obj){
    const value = obj[key]
    if (obj.hasOwnProperty(key) && value !== undefined) {
      formData.append(key, value)
    }
  }
  return formData
}
