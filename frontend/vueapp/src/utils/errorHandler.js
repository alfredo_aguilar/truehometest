
import makeErrorMessage from './makeErrorMessage'

export default (promise, opts={}) => 
	promise.catch(error => {
		const errorString = error.toString()
		console.log("handling errror", {error, errorString})
		error.toString = () => {
			try {
				return makeErrorMessage(error, opts)
      }
			catch(err) {
				console.error("ERROR managing error", err)
				return errorString
			}
		}
		throw error
	})
