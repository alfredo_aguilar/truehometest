
import store from '@/store'

const ERROR_403_PAGE_NAME = 'Access Denied'
const DEBUG = process.env.NODE_ENV !== 'production'


const routerSetup = (router) => {
  router.beforeEach((to, from, next) => {
    if (!to.meta) {
      return next()
    }

    const { meta } = to
    const { ifCan: ifCanDefault, ifInGroup: ifInGroupDefault, ifUserCan, ifUserInGroup } = meta
    const ifCan = ifCanDefault || ifUserCan
    const ifInGroup = ifInGroupDefault || ifUserInGroup

    if (!ifCan && !ifInGroup) {
      if (to.name != ERROR_403_PAGE_NAME) {
        console.warn(`NO PERMISSIONS FOR ROUTE: "${to.name}"`, { meta, to })
      }
      return next()
    }


    const entries = Object.entries(ifCan || {})

    console.log(`CHECKING NEW PERMISSIONS ${entries}`)

    for (const [permission, entry] of entries) {
      let appModels = entry
      if (!Array.isArray(appModels)) {
        appModels = [appModels]
      }

      appModels = appModels.map(s => s.split('/'))

      for (const [app, model] of appModels) {
        if (!store.getters['account/userAccountCan'](permission, app, model)) {
          console.error('user does not have permission', { permission, app, model })
          // next({
            // name: 'Page403'
          // })
          const params = DEBUG
            ? { description: '', from: to.fullPath, permission, app, model }
            : { description: 'Acceso denegado' }
          if (from.name === ERROR_403_PAGE_NAME &&
              from.query.from === params.from) {
            console.warn("From an Error page", { params, from, to })
            return next({
              name: ERROR_403_PAGE_NAME,
              query: {
                description: !DEBUG ? '' : 'GET OUT'
              }
            })
          } else {
            return next({ name: ERROR_403_PAGE_NAME, query: params })
          }
        }
      }
    }

    if (ifInGroup) {
      const groups = Array.isArray(ifInGroup) ? ifInGroup : [ifInGroup]
      const belongsToAGroup = groups.some(group =>
        store.getters['account/userInGroup'](group))
      if (!belongsToAGroup) {
        console.warn('user not belongs to any of the groups', groups)
        return next({
          name: 'Page403'
        })
      }
    }

    return next()
  })
}

export default routerSetup
