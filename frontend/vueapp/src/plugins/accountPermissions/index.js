
import permissionsMixin from './permissionsMixin'
import { mapGetters } from 'vuex'
import routerPermissionsSetup from './routerSetup'
import mapPermissions from './mapPermissions'


export default {
  install(Vue) {
    Vue.use(permissionsMixin)
  }
}

export {
  routerPermissionsSetup,
  mapPermissions
}
