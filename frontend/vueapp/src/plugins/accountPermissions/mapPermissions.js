
import store from '@/store'

const capitalize = (string) =>
  string.charAt(0).toUpperCase() + string.slice(1)

const toCamel = (s) => {
  return s.replace(/([-_][a-z])/ig, ($1) => {
    return $1.toUpperCase()
      .replace('-', '')
      .replace('_', '')
  })
}

const format = (s) => toCamel(capitalize(s))

const ALL_PERMISSIONS = [ 'view', 'change', 'add', 'delete' ]

const mapPermissions = (app, models, permissions = ALL_PERMISSIONS, opts = {}) => {
  let { prefix } = opts
  prefix = prefix || 'can'

  let modelMap = models

  if (typeof modelMap === 'string') {
    modelMap = [ modelMap ]
  }

  if (Array.isArray(modelMap)) {
    const newMap = {}
    modelMap.forEach(model => {
      newMap[model] = format(model)
    })
    modelMap = newMap
  }

  const obj = {}
  Object.entries(modelMap).forEach(([ model, mappedName ]) => {
    permissions.forEach(p => {
      const getter = () => {
        const appGetter = store.getters['account/modelPermission'](app, model)
        return !!appGetter[p]
      }
      obj[`${prefix}${format(p)}${mappedName}`] = getter
    })
  })

  return obj
}

export default mapPermissions
