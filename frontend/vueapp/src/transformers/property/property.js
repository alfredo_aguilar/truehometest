 import Transformer from '../transformer'
 
 class propertyTransformer extends Transformer {

   static fetch(property) {
     return {
        'id': property.id,
        'title': property.title,
        'address': property.address
    }
   }
 }
 
 export default propertyTransformer
 