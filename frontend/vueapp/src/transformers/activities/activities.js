 import Transformer from '../transformer'
 import moment from 'moment'

 class activityTransformer extends Transformer {

   static fetch(activity) {
     const prop = { ...activity.property }
     const startDate = moment(new Date(activity.schedule))

     return {
       'id': activity.id,
       'name': activity.title,
       'status': activity.status,
       'condition': activity.condition,
       'survey': activity.survey,
       'start': startDate,
       'created_at': activity.created_at,
       'property': {
            'id': prop.id,
            'title': prop.title,
            'address': prop.address,
       },
     }
   }

   static send(activity) {
     return {
        'title': activity.title,
        'answers': activity.answers,
        'property': activity.property,
        'schedule': activity.schedule
     }
   }

   static update(new_date) {
     return {
        'schedule': new_date
     }
   }
 }
 
 export default activityTransformer
 