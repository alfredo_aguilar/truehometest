/* ============
 * Account Transformer
 * ============
 *
 * The transformer for the account.
 */

import Transformer from './transformer'
import { assetURL } from '@/utils'
import GroupTransformer from '@/plugins/accountPermissions/group'
import { reducePermissions } from '@/plugins/accountPermissions/permissions'

export default class AccountTransformer extends Transformer {
  /**
   * Method used to transform a fetched account
   *
   * @param account The fetched account
   *
   * @returns {Object} The transformed account
   */
  static fetch(account) {
    const userType = account.user_type
    const role = userType.role
    const { user } = account

    // django groups
    const groupsData = GroupTransformer.fetchCollection(user.groups)
    const groups = groupsData.map(g => {
      const { permissions, ...data } = g
      return data
    })

    const permissions = reducePermissions(groupsData.map(g => g.permissions))

    return {
      userId: user.id || 0,
      username: user.username || '',
      treatment: user.treatment || 'C.',
      firstName: user.first_name || 'NoName',
      lastName: user.last_name || 'NoLastName',
      secondLastName: user.mothers_last_name || '',
      email: user.email || '',
      // Profile information
      profileId: account.profile.id || 0,
      avatar: assetURL(account.profile.photo) || require('@/assets/images/faces/avatar.jpg'),
      phone: account.profile.phone || '',
      phoneExt: account.profile.phone_ext || '',
      cellphone: account.profile.cellphone || '',
      alternativeEmail: account.profile.alternative_email || '',
      address: account.profile.address || {},
      // Groups and permissions
      userArea: account.user_type.area.name || '',
      userRole: role.name || '',
      userRoleCode: role.code,
      userType: account.user_type.user_type || '',
      groups: user.groups.map((g) => { return { name: g.name } }),
      permissions
    }
  }

  /**
   * Method used to transform a send account
   *
   * @param account The account to be send
   *
   * @returns {Object} The transformed account
   */
  static send(account) {
    return {
      id: account.userId,
      user_name: account.username,
      treatment: account.treatment,
      first_name: account.firstName,
      last_name: account.lastName,
      mothers_last_name: account.secondLastName,
      email: account.email,
      avatar: account.avatar,
    }
  }

  /**
   * Update User data
   */
  static update(account) {
    let serialized = {
      user: {
        id: account.userId,
        username: account.username,
        treatment: account.treatment,
        first_name: account.firstName,
        last_name: account.lastName,
        mothers_last_name: account.secondLastName,
        email: account.email,
      },
      profile: {
        // photo: account.avatar,
        id: account.profileId,
        address: account.address,
        phone: account.phone,
        phone_ext: account.phoneExt,
        cellphone: account.cellphone,
        alternative_email: account.alternativeEmail,
      },
    }

    const photo = account.avatar
    if (photo !== undefined && (typeof photo !== 'string' || photo.startsWith('data:image'))) {
      serialized.profile.photo = photo
    }

    return serialized
  }
}
