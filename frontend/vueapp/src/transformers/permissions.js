/* ============
 * Account Transformer
 * ============
 *
 * The transformer for the account permissions.
 */

import Transformer from './transformer'
import { assetURL } from '@/utils'
import { groupBy } from 'lodash'

const POSTFIX_VERBS = [ 'Get', 'Post', 'PostPut', 'Delete', 'Roles' ]

const CAN_VERBS = [ 'Can' ]

const PREFIX_VERBS = [
  'CreateEditList', 'ListEdit', 'CreateEdit',
  'RejectAuthorize',
  'AuthorizeAndGenerate',
  // simples go last
  'ClientApprove',
  'Reject', 'Register', 
  'Authorize', 'Cancel', 'Approve',
  'Create', 'Edit', 'List', 'Delete', 
  'View',
  'Check'
]

const REGEX_VERB = new RegExp(`^(?<name>.+?)(?:Permission)?(?<verb>${POSTFIX_VERBS.join('|')})$`)
const PRE_REGEX_VERB = new RegExp(`^(?:${CAN_VERBS.join('|')})(?<verb>${PREFIX_VERBS.join('|')})(?<name>.+?)(?:Permission)?$`)

const CAN_REGEX = `^(${CAN_VERBS.join('|')})(.+)$`

const extractMatches = (permissions, REGEX) =>
  Object.fromEntries(
    Object.entries(
      groupBy(
        permissions
          .map(permission => (
            typeof permission === 'string'
            ? { name: permission }
            : { 
              ...permission
              }))
          .map(({ name: permission, ...otherStuff }) => ({
            match: REGEX.exec(permission), permission, ...otherStuff
          }))
          .map(({ match, permission, ...moreInfo }) => ({
            name: match ? match.groups.name : permission,
            verb: match && match.groups.verb,
            permission,
            ...moreInfo
          })),

        ({ name }) => name
      )
    ).map(([ name, verbs ]) => ([
      name,
      {
        verbs: verbs
          .map(({ verb, permission }) => ({ verb, permission }))
          .filter(({ verb, permission })  => !!verb),
        allVerbs: verbs
      }
    ]))
  )

const parseResults = (...args) => {
  const matches = extractMatches(...args)
  const matchesEntries = Object.entries(matches)

  const matched = Object.fromEntries(
    matchesEntries.filter(([ name, { verbs } ]) => verbs.length > 0)
  )
  const other = matchesEntries
    .filter(([ name, { verbs } ]) => verbs.length === 0)
    .map(([ name ]) => name)

  return {
    allMatches: matches,
    matched,
    other
  }
}


export {
  POSTFIX_VERBS
}

const classicPermissions = (accountPermissions) => {
  const { user, user_type: userType, permissions } = accountPermissions

  // const matches = extractMatches(permissions, REGEX_VERB)
  const { allMatches: matches, matched: postResourceMatches, other: otherMatches } = parseResults(permissions, REGEX_VERB)

  // const postResourceMatches = Object.fromEntries(
  //   Object.entries(matches).filter(([ name, { verbs } ]) => verbs.length > 0)
  // )
  // const otherMatches = Object.entries(matches)
  //   .filter(([ name, { verbs } ]) => verbs.length === 0)
  //   .map(([ name ]) => name)

  const canPermissionsTest = otherMatches
    .map(permission => ({
      permission,
      match: permission.match(CAN_REGEX)
    }))

  const nonCanMatches = canPermissionsTest
    .filter(({ permission, match }) => !match)
    .map(({ permission }) => ({
      permission,
      verb: permission
    }))

  const canPermissions = canPermissionsTest
    .filter(({ permission, match }) => match)
    .map(({ match, permission }) => ({ match: match[2], permission }))


  const { allMatches: allCanMatches, matched: preResourceMatches, other: restingMatches } = parseResults(
    canPermissions
    .map(({ permission, match, ...otherStuff }) => ({
      name: permission, label: match, permission, ...otherStuff
    })),
    PRE_REGEX_VERB
  )

  const canPermissionsMatches = Object.entries(
    allCanMatches
  )
  // const preResourceMatches = Object.fromEntries(
  //   canPermissionsMatches.filter(([ name, { verbs } ]) => verbs.length > 0)
  // )


  const otherCanMatches = canPermissionsMatches
    .filter(([ name, { verbs } ]) => verbs.length === 0)
    .map(([ name, { label, permission, allVerbs } ]) => ({
      label: label || name,
      verb: permission || label || name,
      permission: allVerbs
      .map(({ permission, label, match }) => (label || match || permission))
      .join(', ')
    }))

  // const restingMatches = canPermissionsMatches
  //   .filter(([ name, { verbs } ]) => verbs.length === 0)
  //   .map(([ name ]) => name)

  const mixed = Object.fromEntries(
    Object.entries(preResourceMatches)
    .filter(([ resource ]) => !!postResourceMatches[resource])
    .map(([ resource, values ]) => (
      [ resource, [ ...values, ...postResourceMatches[resource] ] ]
    ))
  )

  return Object.fromEntries([
    ...[
      ...Object.entries({
        ...mixed,
        canMatches: otherCanMatches,
        more: nonCanMatches
      }
      ),
      ...Object.entries(postResourceMatches)
      .map(([ resource, { verbs }]) => ([ resource, verbs ])),
      ...Object.entries(preResourceMatches)
      .map(([ resource, { verbs }]) => ([ resource, verbs ])),
    ].sort(([ name, values], [otherName, otherValues]) => name < otherName ? -1 : 1),
  ])
}

export default class AccountPermissionsTransformer extends Transformer {
  static fetch(accountPermissions) {
    return {
      ...classicPermissions(accountPermissions),
    }
  }
}
