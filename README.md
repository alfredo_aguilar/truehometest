# Django

Simple project configuration for Django

## Installation

Clone el proyecto

1. Creé Virtual env
```bash
python -m venv .venv
```

2. Instale las dependencias
```bash
# For development
pip install -r requirements.txt
```

3. Configura tu base de datos
```bash
En este ejemplo, una base de datos de Postgres ya está configurada solo para el entorno de producción.
```

4. Crea un archivo ```.env``` en el directorio root. Esto manejará sus variables de configuración.
Ejemplo:
```
ENVIRONMENT=dev
SECRET_KEY=N0t_A_S3Cr3T
DEBUG=True
DB_URL=postgres://USER:PASSWORD@HOST/DB_NAME
LANGUAGE_CODE=es-mx
TIME_ZONE=America/Mexico_City
DEFAULT_EMAIL_FROM=sender@mail.com
SERVER_EMAIL=server.com
EMAIL_HOST=
EMAIL_PORT=465
EMAIL_HOST_USER=email_user
EMAIL_HOST_PASSWORD=email_passwd
EMAIL_USE_TLS=True
```

Para la variable `ENVIRONMENT`, los valores pueden ser: `dev` o `production`

5. Corre migraciones
```bash
python manage.py makemigrations
python manage.py migrate
```

6. Crea un super usuario
```bash
python manage.py createsuperuser
```

Puede ser que quiera crear un super usuario desde GitBash, para ese caso se usa el comando
```
winpty python manage.py createsuperuser
```

7. Para correr el frontend
    ```
    cd frontend/vueapp/
    ```

    ```
    npm i
    ```

    ```
    npm run dev
    ```
