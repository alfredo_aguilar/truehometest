from django.contrib import admin
from django.urls import path
from django.urls import path, include

from rest_framework_jwt.views import verify_jwt_token
from rest_framework_jwt.views import obtain_jwt_token, refresh_jwt_token


urlpatterns = [
    path('admin/', admin.site.urls),
    # JWT auth
    path('api/v1/auth/obtain_token/', obtain_jwt_token),
    path('api/v1/auth/refresh_token/', refresh_jwt_token),
    path('api/v1/auth/token-verify/', verify_jwt_token),
    # Users
    path(
        'api/v1/user/',
        include(
            ('apps.users.urls', 'Users'),
            namespace='Users'
        )
    ),
    path(
        'api/v1/activities/',
        include(
            ('apps.activities.urls', 'Activities'),
            namespace='Activities'
        )
    ),
]
