#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Django 
from django.contrib import admin

# Models
from .models import Activity, Survey, Property

VERSION = (0, 0, 2)
__author__ = 'Alfredo Ricardo Aguilar Hernandez'
__email__ = 'aguilarhernandeza@gmail.com'
__date__ = '08/12/2021'
__modified__ = '08/12/2021'
__version__ = '.'.join([str(x) for x in VERSION])

# Register your models here.
admin.site.register(Activity)
admin.site.register(Survey)
admin.site.register(Property)