#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Django
from django.db import models

VERSION = (0, 0, 2)
__author__ = 'Alfredo Ricardo Aguilar Hernandez'
__email__ = 'aguilarhernandeza@gmail.com'
__date__ = '07/12/2021'
__modified__ = '07/12/2021'
__version__ = '.'.join([str(x) for x in VERSION])

# Status for models
STATUS = (
    ('active', 'Activo'),
    ('disabled', 'Desactivado'),
    ('done', 'Realizada')
)

class Property(models.Model):
    title = models.CharField(
        verbose_name='Titulo',
        max_length=255
    )
    address = models.TextField(
        verbose_name='Direccion',
    )
    description = models.TextField(
        verbose_name='Descripcion'
    )
    created_at = models.DateTimeField(
        verbose_name='Creado en',
        auto_now_add=True
    )
    updated_at = models.DateTimeField(
        verbose_name='Actualizado en',
        auto_now=True
    )
    disabled_at = models.DateTimeField(
        verbose_name='Deshabilitado en',
        null=True, blank=True
    )
    status = models.CharField(
        verbose_name='Estado',
        max_length=35, choices=STATUS
    )

    def __str__(self):
        return "Propiedad: {} Estado: {}".format(self.title, self.status)

    class Meta:
        verbose_name = 'Propiedad'
        verbose_name_plural = 'Propiedades'


class Activity(models.Model):
    property = models.ForeignKey(
        Property, verbose_name='Propiedad',
        on_delete=models.CASCADE
    )
    schedule = models.DateTimeField(
        verbose_name='Fecha de agendado',
    )
    title = models.CharField(
        verbose_name='Titulo',
        max_length=255
    )
    created_at = models.DateTimeField(
        verbose_name='Creado en',
        auto_now_add=True
    )
    updated_at = models.DateTimeField(
        verbose_name='Actualizado en',
        auto_now=True
    )
    status = models.CharField(
        verbose_name='Estado',
        max_length=35, choices=STATUS
    )

    def __str__(self):
        return "Actividad: {} Estado: {}".format(self.title, self.status)

    class Meta:
        verbose_name = 'Actividad'
        verbose_name_plural = 'Actividades'


class Survey(models.Model):
    activity = models.ForeignKey(
        Activity, verbose_name='Actividad',
        on_delete=models.CASCADE, related_name='activity_survey'
    )
    answers = models.JSONField(
        verbose_name='Preguntas'
    )
    created_at = models.DateTimeField(
        verbose_name='Creado en',
        auto_now_add=True
    )

    def __str__(self):
        return "Actividad: {}".format(self.activity.title)

    class Meta:
        verbose_name = 'Encuesta'
        verbose_name_plural = 'Encuestas'
