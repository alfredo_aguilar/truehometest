#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Django
from django.urls import path

# Views
from .views import (
    ActivityListCreateView, ReScheduleActivityView, DisableActivityView,
    SurveyListView, PropertyListView
)

VERSION = (0, 0, 2)
__author__ = 'Alfredo Ricardo Aguilar Hernandez'
__email__ = 'aguilarhernandeza@gmail.com'
__date__ = '23/12/2020'
__modified__ = '23/12/2020'
__version__ = '.'.join([str(x) for x in VERSION])


urlpatterns = [
    path(
        '',
        ActivityListCreateView.as_view(),
        name='activities-list-create'
    ),
    path(
        're-schedule/<int:id>/',
        ReScheduleActivityView.as_view(),
        name='activities-re-schedule'
    ),
    path(
        'cancel/<int:id>/',
        DisableActivityView.as_view(),
        name='activities-cancellation'
    ),
    path(
        'survey/<int:id>/',
        SurveyListView.as_view(),
        name='activities-cancellation'
    ),
    path(
        'properties/',
        PropertyListView.as_view(),
        name='activities-cancellation'
    ),
]