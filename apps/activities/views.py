#!/usr/bin/env python
# -*- coding: utf-8 -*-

# utils
from datetime import timedelta
from django.utils import timezone

# Django
from django.db.models import Q

# Django REST Framework
from rest_framework import status as st
from rest_framework.response import Response

# Serializers
from .serializers import (
    ActivityCreateSerializer, ReScheduleActivitySerializer,
    ActivityListSerializer, SurveyListSerializer,
    PropertyForActivityListSerializer
)

# Models
from .models import Activity, Property, Survey

# generic views
from apps.utils.APIView.generics import (
    ListCreateAPIView, UpdateAPIView, SoftDestroyAPIView, RetrieveAPIView,
    ListAPIView
)

VERSION = (0, 0, 2)
__author__ = 'Alfredo Ricardo Aguilar Hernandez'
__email__ = 'aguilarhernandeza@gmail.com'
__date__ = '08/12/2021'
__modified__ = '08/12/2021'
__version__ = '.'.join([str(x) for x in VERSION])


class PropertyListView(ListAPIView):
    queryset = Property.objects.all()
    serializer_class = PropertyForActivityListSerializer


class SurveyListView(RetrieveAPIView):
    queryset = Survey.objects.all()
    serializer_class = SurveyListSerializer
    lookup_field = 'id'


class ActivityListCreateView(ListCreateAPIView):
    queryset = Activity.objects.filter(status="active")
    serializer_class = ActivityCreateSerializer
    serializer_list = ActivityListSerializer

    def get(self, request, *args, **kwargs):
        # parameters sent through the GET
        schedule_from = request.GET.get('from', None)
        schedule_to = request.GET.get('to', None)
        status = request.GET.get('status', None)

        # original query
        query = self.get_queryset()

        # start schedule filters ---------------------------------------
        today = timezone.now()
        date_from = None
        date_to = None

        if schedule_from is None and schedule_to is None:
            date_from = today - timedelta(days=3)
            date_to = today + timedelta(weeks=2)
        elif schedule_from is not None and schedule_to is None:
            date_from = datetime.datetime.strptime(
                schedule_from, "%Y/%m/%d"
            ).date()
            date_to = today
        elif schedule_from is None and schedule_to is not None:
            date_from = today
            date_to = datetime.datetime.strptime(
                schedule_to, "%Y/%m/%d"
            ).date()

        # Here the date filters defined with the previous conditions are applied
        query = query.filter(
            schedule__gte=date_from, schedule__lte=date_to
        )

        # start status filters -----------------------------------------
        if status is not None:
            query = query.filter(
                status__icontains=status
            )

        serialized = ActivityListSerializer(query, many=True)
        return Response(serialized.data, status=st.HTTP_200_OK)


class ReScheduleActivityView(UpdateAPIView):
    queryset = Activity.objects.all()
    serializer_class = ReScheduleActivitySerializer
    lookup_field = 'id'


class DisableActivityView(SoftDestroyAPIView):
    queryset = Activity.objects.all()
    serializer_class = None
    lookup_field = 'id'
