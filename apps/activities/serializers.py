#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Django
from django.db.models import F
from django.utils import timezone

# utils
from datetime import timedelta

# Models
from .models import Activity, Property, Survey

# Serializers
from rest_framework.serializers import (
    SerializerMethodField, ModelSerializer, JSONField
)
from rest_framework.validators import ValidationError

VERSION = (0, 0, 2)
__author__ = 'Alfredo Ricardo Aguilar Hernandez'
__email__ = 'aguilarhernandeza@gmail.com'
__date__ = '08/12/2021'
__modified__ = '08/12/2021'
__version__ = '.'.join([str(x) for x in VERSION])

class PropertyForActivityListSerializer(ModelSerializer):

    class Meta:
        model = Property
        fields = ['id', 'title', 'address']


class SurveyListSerializer(ModelSerializer):

    class Meta:
        model = Survey
        fields = ['answers', ]


class ActivityListSerializer(ModelSerializer):
    property = PropertyForActivityListSerializer()
    condition = SerializerMethodField()
    survey = SerializerMethodField()

    class Meta:
        model = Activity
        exclude = ('updated_at', )

    def get_condition(self, obj):
        condition = ""
        today = timezone.now()
        if obj.status == 'active' and obj.schedule >= today:
            condition = 'Pendiente a realizar'
        elif obj.status == 'active' and obj.schedule <= today:
            condition = 'Atrasada'
        elif obj.status == 'done':
            condition = 'Finalizada'
        return condition

    def get_survey(self, obj):
        try:
            survey = Survey.objects.get(activity=obj)
            return "http://127.0.0.1:8000/api/v1/activities/survey/{}/".format(
                survey.id
            )
        except Survey.DoesNotExist:
            return "No hay encuesta relacionada"


class ActivityCreateSerializer(ModelSerializer):
    answers = JSONField(write_only=True, required=True)

    class Meta:
        model = Activity
        fields = '__all__'
        read_only_fields = ('status', 'updated_at', 'created_at')

    def validate(self, data):
        prop = data.get('property')
        scheduled_date = data.get('schedule')
        if prop.status == 'disabled':
            raise ValidationError("La propiedad no esta activa")

        date = scheduled_date.date()
        hour_from = scheduled_date.hour
        hour_to = (scheduled_date + timedelta(hours=1)).hour

        if Activity.objects.filter(
            schedule=date,
            schedule__hour__gte=hour_from,
            schedule__hour__lte=hour_to,
            property=prop,
            status="active"
            ).exists():
            raise ValidationError(
                "Ya existe una actividad para el mismo dia y hora"
            )
        return data

    def create(self, validated_data):
        answers = validated_data.pop('answers')
        created = Activity.objects.create(**validated_data, status='active')
        Survey.objects.create(
            activity=created,
            answers=answers
        )
        return created



class ReScheduleActivitySerializer(ModelSerializer):

    class Meta:
        model = Activity
        fields = ('schedule', )

    def update(self, instance, validated_data):
        prop = instance.property
        scheduled_date = validated_data.get('schedule')

        if instance.status == 'disabled':
            raise ValidationError(
                "No se pueden re-agendar actividades canceladas."
            )

        date = scheduled_date.date()
        hour_from = scheduled_date.hour
        hour_to = (scheduled_date + timedelta(hours=1)).hour

        if Activity.objects.filter(
            schedule=date,
            schedule__hour__gte=hour_from,
            schedule__hour__lte=hour_to,
            property=prop,
            status="active"
            ).exists():
            raise ValidationError(
                "Ya existe una actividad para el mismo dia y hora"
            )

        return super().update(instance, validated_data)
