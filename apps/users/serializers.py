#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Django
from django.contrib.auth import update_session_auth_hash
from django.contrib.auth.models import Group, Permission

# Serializers
from rest_framework.serializers import (
    Serializer,
    CharField, ValidationError,
    SerializerMethodField, ModelSerializer
)


# Models
from .models import AppUser

# Utilities
from apps.utils.utils import Base64ImageField

VERSION = (0, 0, 2)
__author__ = 'Alfredo Ricardo Aguilar Hernandez'
__email__ = 'aguilarhernandeza@gmail.com'
__date__ = '23/12/2020'
__modified__ = '23/12/2020'
__version__ = '.'.join([str(x) for x in VERSION])


class PermissionSerializer(ModelSerializer):
    """ Permission serializer """
    perms = SerializerMethodField()

    def get_perms(self, obj):
        per = '{}/{}/{}'.format(
            obj.content_type.app_label,
            obj.content_type.model,
            obj.codename.split('_')[0]
        )
        return per

    class Meta:
        model = Permission
        fields = ('perms', )
        # fields = ('name', 'codename')


class GroupSerializer(ModelSerializer):
    """ Group serializer """
    # permissions = PermissionSerializer(many=True, read_only=True)
    permissions = SerializerMethodField()

    def get_permissions(self, obj):
        perms = list()
        for perm in obj.permissions.iterator():
            perms.append('{}/{}/{}'.format(
                perm.content_type.app_label,
                perm.content_type.model,
                perm.codename.split('_')[0]
            ))
        return perms

    class Meta:
        model = Group
        fields = ('name', 'permissions')


class UserSerializer(ModelSerializer):
    """ User serializer """
    groups = GroupSerializer(many=True, read_only=True)

    class Meta:
        model = AppUser
        extra_kwargs = {
            'is_active': {'read_only': True},
            'is_staff': {'read_only': True},
            'is_superuser': {'read_only': True},
            'user_permissions': {'read_only': True},
            'date_joined': {'read_only': True},
            'username': {'read_only': True},
        }
        exclude = ('password', 'is_superuser', 'is_staff')


class MinimalUserSerializer(ModelSerializer):
    """ Minimal custom user serializer """
    class Meta:
        model = AppUser
        fields = (
            'username', 'first_name', 'last_name', 'mothers_last_name',
            'treatment'
        )


class AppUserSerializer(ModelSerializer):
    """ Rack1 user serializer """
    user = UserSerializer(read_only=True)

    class Meta:
        model = AppUser
        fields = '__all__'


class BasicAppUserSerializer(ModelSerializer):
    """ Basic user serializer """
    user = MinimalUserSerializer()

    class Meta:
        model = AppUser
        fields = ('user', 'photo')


class ChangePasswordSerializer(Serializer):
    """ Change password serializer """
    old_password = CharField(required=True, max_length=30)
    password = CharField(required=True, max_length=30)
    confirmed_password = CharField(required=True, max_length=30)

    def validate(self, data):
        if not self.context['request'].user.check_password(data.get('old_password')):  # noqa
            raise ValidationError(
                {'contraeña anterior': 'La contraseña no coincide con la registrada'}  # noqa
            )

        if data.get('confirmed_password') != data.get('password'):
            raise ValidationError(
                {'confirmar contraseña': 'Las contraseñas no coinciden'}
            )

        return data

    def update(self, instance, validated_data):
        instance.set_password(validated_data['password'])
        instance.save()
        update_session_auth_hash(self.context['request'], instance)
        return instance

    def create(self, validated_data):
        pass

    @property
    def data(self):
        return {'Success': True}


class AppUserUpdateSerializer(ModelSerializer):
    """ rack1 user serializer """
    user = UserSerializer()
    photo = Base64ImageField(required=False)

    def update(self, instance, validated_data):
        """ Override upate method """
        user = validated_data.get('user')

        # Current data to modify
        current_user = instance.user

        # Update User
        updated_user = UserSerializer(current_user, data=user)
        if updated_user.is_valid():
            updated_user.save()
        else:
            pass
        # Rack1User save
        instance.user = current_user
        updated = Rack1UserSerializer(instance, data=validated_data)
        updated.is_valid(raise_exception=True)
        updated.save()

        return instance

    class Meta:
        model = AppUser
        fields = '__all__'
