#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Django
from django.urls import path

# Views
from .views import (
    GetUserAPI,
    CurrentUser,
    ListUsersAPI,
    GetUserByGroup,
    APIChangePasswordView
)

VERSION = (0, 0, 2)
__author__ = 'Alfredo Ricardo Aguilar Hernandez'
__email__ = 'aguilarhernandeza@gmail.com'
__date__ = '23/12/2020'
__modified__ = '23/12/2020'
__version__ = '.'.join([str(x) for x in VERSION])


urlpatterns = [
    path(
        'current/',
        CurrentUser.as_view(),
        name='current-user'
    ),
    # Change password
    path(
        'current/change_password/',
        APIChangePasswordView.as_view(),
        name='change-passwd'
    ),
    # Admin Actions
    path(
        '',
        ListUsersAPI.as_view(),
        name='list-users'
    ),
    path(
        '<int:pk>/',
        GetUserAPI.as_view(),
        name='user-details'
    ),
    path(
        '<slug:group>/',
        GetUserByGroup.as_view(),
        name='users-by-role'
    ),
]
