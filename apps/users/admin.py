#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Django
from django.contrib import admin
from django.contrib.auth import get_user_model
from django.contrib.auth.admin import UserAdmin


VERSION = (0, 0, 1)
__author__ = 'Alfredo Ricardo Aguilar Hernandez'
__email__ = 'aguilarhernandeza@gmail.com'
__date__ = '23/12/2020'
__modified__ = '23/12/2020'
__version__ = '.'.join([str(x) for x in VERSION])


class AppUserAdmin(UserAdmin):
    search_fields = ('username', 'email')
    list_display = (
        'username', 'email', 'first_name', 'last_name',
        'mothers_last_name', 'is_staff', 'is_active'
    )

admin.site.register(get_user_model(), AppUserAdmin)
