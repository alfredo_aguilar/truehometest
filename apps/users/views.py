#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Django
from django.db import transaction

# Django REST Framework
from rest_framework import status
from rest_framework import generics
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.permissions import (
    IsAuthenticated, IsAdminUser
)

# Serializers
from .serializers import (
    AppUserSerializer, ChangePasswordSerializer, AppUserUpdateSerializer
)

# Models
from .models import AppUser

VERSION = (0, 0, 2)
__author__ = 'Alfredo Ricardo Aguilar Hernandez'
__email__ = 'aguilarhernandeza@gmail.com'
__date__ = '23/12/2020'
__modified__ = '23/12/2020'
__version__ = '.'.join([str(x) for x in VERSION])


class ListUsersAPI(generics.ListCreateAPIView):
    """ List and register users """
    queryset = AppUser.objects.all()
    serializer_class = AppUserSerializer
    permission_classes = (IsAuthenticated, IsAdminUser)


class GetUserAPI(generics.RetrieveUpdateDestroyAPIView):
    """ Retrieve, Update and Delete user """
    queryset = AppUser.objects.all()
    serializer_class = AppUserSerializer
    permission_classes = (IsAuthenticated, IsAdminUser)

    def delete(self, request, pk):
        """ Override delete method to inactivate user """
        obj = self.get_object(pk)
        obj.user.is_active = False
        obj.user.save()
        obj.save()
        return Response(
            {'message': 'Eliminado'}, status=status.HTTP_200_OK
        )


class GetUserByGroup(generics.ListAPIView):
    """ Get users by Group/role """
    queryset = AppUser.objects.all()
    serializer_class = AppUserSerializer

    def list(self, request, group):
        """ Returns users by group """
        users = AppUser.objects.filter(user__groups__name=group)
        serializer = AppUserSerializer(users, many=True)
        return Response(serializer.data)


class APIChangePasswordView(generics.UpdateAPIView):
    """ API Change Password View """
    serializer_class = ChangePasswordSerializer
    model = AppUser
    permission_classes = (IsAuthenticated,)

    def get_object(self, queryset=None):
        return self.request.user


class CurrentUser(APIView):
    """ Current user APIView """
    permission_classes = (IsAuthenticated, )

    def get(self, request):
        """ Get Method
        Get information about current user
        """
        try:
            user = AppUser.objects.get(user=request.user)
            serializer = AppUserSerializer(user)
            return Response(serializer.data)
        except AppUser.DoesNotExist:
            return Response(
                {'message': 'Usuario no encontrado'},
                status=status.HTTP_400_BAD_REQUEST
            )

    @transaction.atomic
    def put(self, request):
        """ PUT Method
        Modify user information
        """
        user = AppUserUpdateSerializer(
            AppUser.objects.get(user=request.user),
            data=request.data
        )
        user.is_valid(raise_exception=True)
        user.save()
        return Response(user.data, status=status.HTTP_200_OK)
