#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Django
from django.db import models
from django.conf import settings
from django.utils.translation import ugettext as _
from django.contrib.auth.models import AbstractUser, Permission

# Utilities
# from auditlog.registry import auditlog

# Models


VERSION = (0, 0, 2)
__author__ = 'Alfredo Ricardo Aguilar Hernandez'
__email__ = 'aguilarhernandeza@gmail.com'
__date__ = '23/12/2020'
__modified__ = '23/12/2020'
__version__ = '.'.join([str(x) for x in VERSION])

COMMON_USER_DEFAULT_PHOTO = settings.COMMON_USER_DEFAULT_PHOTO


class AppUser(AbstractUser):
    """ Aurora User Model"""

    TREATMENT_CHOICES = (
        ('C.', _('C.')),
        ('Lic.', _('Lic.')),
        ('Ing.', _('Ing.')),
        ('Mtro.', _('Mtro.')),
        ('Dr.', _('Dr.')),
    )
    USERNAME_FIELD = 'username'

    mothers_last_name = models.CharField(
        verbose_name=_('Apellido Materno'), max_length=30, blank=True
    )
    phone = models.CharField(
        verbose_name=_('Teléfono'),
        null=False, blank=False,
        max_length=10
    )
    cellphone = models.CharField(
        verbose_name=_('Teléfono Celular'),
        null=False, blank=True,
        max_length=50
    )
    photo = models.ImageField(
        verbose_name=_('Imágen de Perfil'),
        upload_to='common_user/photos/',
        default=COMMON_USER_DEFAULT_PHOTO,
        null=True, blank=True, max_length=200
    )
    treatment = models.CharField(
        verbose_name=_('Título'), max_length=5,
        choices=TREATMENT_CHOICES,
        blank=True, null=True, default=_('C.')
    )

    def get_full_name(self):
        """
        Returns the first_name, the last_name, the mothers_last_name
        with a space in between.
        """
        full_name = '{} {} {} {}'.format(
            self.treatment or 'C.', self.first_name, self.last_name,
            self.mothers_last_name
        )
        return full_name.strip()

    def get_first_name_and_first_lastname(self):
        """
        Returns the first_name plus the last_name, with a space in between.
        """
        full_name = '{} {}'.format(self.first_name, self.last_name)
        return full_name.strip()

    def get_complete_name(self):
        """
        Returns the first_name plus the last_name, with a space in between.
        """
        full_name = '{} {} {}'.format(
            self.first_name, self.last_name, self.mothers_last_name
        )
        return full_name.strip()

    def get_user_permissions(self):
        """
        Returns related permissions
        """
        return Permission.objects.filter(user=self)

    def __str__(self):
        name = self.get_full_name() if self.first_name else self.username
        return name

    class Meta:
        verbose_name = _('Usuario')
        verbose_name_plural = _('Usuarios')
        ordering = ['-date_joined']


# Auditlog models registration
# auditlog.register(AppUser)
