# Utilities
import uuid
import base64

# REST
from rest_framework.serializers import ImageField

# Django
from django.core.files.base import ContentFile

VERSION = (0, 0, 3)
__author__ = 'Alfredo Aguilar'
__mail__ = 'aguilarhernandeza@gmail.com'
__date__ = '11/11/2021'
__modified__ = '11/11/2021'
__version__ = '.'.join([str(x) for x in VERSION])


class Base64ImageField(ImageField):
    def to_internal_value(self, data):
        if isinstance(data, str) and data.startswith('data:image'):
            # base64 encoded image - decode
            format, imgstr = data.split(';base64,')  # format ~= data:image/X,
            ext = format.split('/')[-1]  # guess file extension
            id = uuid.uuid4()
            data = ContentFile(
                base64.b64decode(imgstr), name=id.urn[9:] + '.' + ext
            )
        elif data == "":
            return None
        return super(Base64ImageField, self).to_internal_value(data)
