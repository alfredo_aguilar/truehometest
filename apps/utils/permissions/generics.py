#!/usr/bin/env python
# -*- coding: utf-8 -*-

from rest_framework import exceptions
# Django REST Framework
from rest_framework.permissions import BasePermission

VERSION = (0, 0, 1)
__author__ = 'David Rodríguez'
__email__ = 'luisrgz2904@gmail.com'
__date__ = '12/02/2020'
__modified__ = '12/02/2020'
__version__ = '.'.join([str(x) for x in VERSION])


def custom_permission(perm_name, methods):
    permition_class = type(
        "{0}PermissionClass ".format(perm_name),
        (GenericPermission,),
        {
            'permission_name': perm_name,
            'authorized_methods': methods
        }
    )
    return permition_class


class GenericPermission(BasePermission):
    """
    A base class from which all permission classes should inherit.
    """

    permission_name = None
    authorized_methods = []

    perms_map = {
        'GET': ['%(app_label)s.%(acion_name)s_%(model_name)s'],
        'POST': ['%(app_label)s.%(acion_name)s_%(model_name)s'],
        'PUT': ['%(app_label)s.%(acion_name)s_%(model_name)s'],
        'PATCH': ['%(app_label)s.%(acion_name)s_%(model_name)s'],
        'DELETE': ['%(app_label)s.%(acion_name)s_%(model_name)s'],
    }

    def get_required_permissions(self, method, model_cls):
        """
        Given a model and an HTTP method, return the list of permission
        codes that the user is required to have.
        """
        kwargs = {
            'app_label': model_cls._meta.app_label,
            'acion_name': self.permission_name,
            'model_name': model_cls._meta.model_name
        }

        if method not in self.perms_map:
            raise exceptions.MethodNotAllowed(method)

        return [perm % kwargs for perm in self.perms_map[method]]

    def _queryset(self, view):
        assert hasattr(view, 'get_queryset') \
            or getattr(view, 'queryset', None) is not None, (
            'Cannot apply {} on a view that does not set '
            '`.queryset` or have a `.get_queryset()` method.'
        ).format(self.__class__.__name__)

        if hasattr(view, 'get_queryset'):
            queryset = view.get_queryset()
            assert queryset is not None, (
                '{}.get_queryset() returned None'.format(view.__class__.__name__)
            )
            return queryset
        return view.queryset

    def has_permission(self, request, view):
        # Workaround to ensure DjangoModelPermissions are not applied
        # to the root view when using DefaultRouter.
        if getattr(view, '_ignore_model_permissions', False):
            return True

        if not request.user or (
           not request.user.is_authenticated and self.authenticated_users_only):
            return False

        queryset = self._queryset(view)
        perms = self.get_required_permissions(request.method, queryset.model)
        if request.method in self.authorized_methods:
            return request.user.has_perms(perms)
        return True
