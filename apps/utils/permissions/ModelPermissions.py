#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Django REST Framework
from rest_framework.permissions import (DjangoModelPermissions)

VERSION = (0, 0, 1)
__author__ = 'Alfredo Ricardo Aguilar Hernandez'
__email__ = 'aguilarhernandeza@gmail.com'
__date__ = '12/02/2020'
__modified__ = '12/02/2020'
__version__ = '.'.join([str(x) for x in VERSION])


class CustomDjangoModelPermissions(DjangoModelPermissions):
    """
    It ensures that the user is authenticated, and has the appropriate
    `view`/`add`/`change`/`delete` permissions on the model.

    This permission can only be applied against view classes that
    provide a `.queryset` attribute.
    """
    def __init__(self):
        self.perms_map['GET'] = ['%(app_label)s.view_%(model_name)s']
