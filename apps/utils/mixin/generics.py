#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Django REST Framework
from rest_framework import status
from rest_framework.response import Response
from rest_framework.settings import api_settings

VERSION = (0, 0, 1)
__author__ = 'David Rodríguez'
__email__ = 'luisrgz2904@gmail.com'
__date__ = '12/02/2020'
__modified__ = '12/02/2020'
__version__ = '.'.join([str(x) for x in VERSION])


class SoftDestroyModelMixin:
    """
    Disable a model instance.
    """
    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        self.perform_destroy(instance)
        return Response(status=status.HTTP_204_NO_CONTENT)

    def perform_destroy(self, instance):
        instance.status = "disabled"
        instance.save()


class ModelDestroyModelMixin:
    """
    Excecute custom delete method in model
    """
    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        instance.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class ListModelMixin:
    """
    List a queryset.
    """
    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())

        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer_list(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer_list(queryset, many=True)
        return Response(serializer.data)


class RetrieveModelMixin:
    """
    Retrieve a model instance.
    """
    def retrieve(self, request, *args, **kwargs):
        instance = self.get_object()
        serializer = self.get_serializer_list(instance)
        return Response(serializer.data)


class UpdateModelMixin:
    """
    Update a model instance.
    """
    def update(self, request, use_response_data=False, *args, **kwargs):
        partial = kwargs.pop('partial', False)
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)
        data = {}

        if getattr(instance, '_prefetched_objects_cache', None):
            # If 'prefetch_related' has been applied to a queryset, we need to
            # forcibly invalidate the prefetch cache on the instance.
            instance._prefetched_objects_cache = {}
        serializer = self.get_response_serializer(self.get_object())
        if use_response_data:
            data = serializer.response_data
        else:
            data = serializer.data
        return Response(data)

    def perform_update(self, serializer):
        serializer.save()

    def partial_update(self, request, use_response_data=False, *args, **kwargs):
        kwargs['partial'] = True
        return self.update(request, use_response_data=use_response_data, *args, **kwargs)


class CreateModelMixin:
    """
    Create a model instance.
    """
    def create(self, request, data=None, use_response_data=False, *args, **kwargs):
        if data is None:
            serializer = self.get_serializer(data=request.data)
        else:
            serializer = self.get_serializer(data=data)
        serializer.is_valid(raise_exception=True)
        instance = self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)

        if use_response_data:
            data = serializer.response_data
        elif self.response_serializer is not None:
            serializer = self.get_response_serializer(instance)
            data = serializer.data
        else:
            data = serializer.data

        return Response(data, status=status.HTTP_201_CREATED, headers=headers)

    def perform_create(self, serializer):
        return serializer.save()

    def get_success_headers(self, data):
        try:
            return {'Location': str(data[api_settings.URL_FIELD_NAME])}
        except (TypeError, KeyError):
            return {}
