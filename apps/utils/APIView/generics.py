#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Django
from django.db.models.query import QuerySet
from django.shortcuts import get_object_or_404
from django.db import transaction
from django.db.models import Q
from django.contrib.contenttypes.models import ContentType
from django.contrib.auth.models import Permission
from django.db.models.fields.reverse_related import (
    ManyToManyRel, ManyToOneRel, OneToOneRel, ForeignObjectRel
    )

# Django REST Framework
from rest_framework import generics
from rest_framework.authentication import SessionAuthentication
from rest_framework.permissions import IsAuthenticated

# Mixins from Utils
from ..mixin.generics import (
    SoftDestroyModelMixin, RetrieveModelMixin,
    ListModelMixin, UpdateModelMixin, CreateModelMixin,
    ModelDestroyModelMixin
)

# serializers
from ..Serializers.generics import DynamicFieldsModelSerializer

# permissions
from apps.utils.permissions.generics import custom_permission

VERSION = (0, 0, 1)
__author__ = 'Alfredo Ricardo Aguilar Hernandez'
__email__ = 'aguilarhernandeza@gmail.com'
__date__ = '12/02/2020'
__modified__ = '12/02/2020'
__version__ = '.'.join([str(x) for x in VERSION])


class CustomGenericAPIView(generics.GenericAPIView):
    """
    Base class for all other custom generic views.
    """
    serializer_list = None
    response_serializer = None
    lookup_field = 'slug'
    lookup_fields = None
    special_permission_classes = []
    fields = []

    def __init__(self):
        if self.special_permission_classes:
            special_perm = []
            for perm in self.special_permission_classes:
                special_perm.append(custom_permission(perm[0], perm[1]))
            if len(self.special_permission_classes) != 0:
                self.permission_classes = (
                    [IsAuthenticated] + special_perm
                )

    def get_queryset(self):
        """
        Get the list of items for this view.
        This must be an iterable, and may be a queryset.
        Defaults to using `self.queryset`.

        This method should always be used rather than accessing `self.queryset`
        directly, as `self.queryset` gets evaluated only once, and those results
        are cached for all subsequent requests.

        You may want to override this if you need to provide different
        querysets depending on the incoming request.

        (Eg. return a list of items that is specific to the user)
        """
        assert self.queryset is not None, (
            "'%s' should either include a `queryset` attribute, "
            "or override the `get_queryset()` method."
            % self.__class__.__name__
        )
        queryset = self.queryset
        kwargs = self.kwargs
        lookup_fields = self.lookup_fields
        filter_kwargs = {}
        if lookup_fields is not None:
            for key in self.lookup_fields.keys():
                # Perform the lookup filtering.
                lookup_url_kwarg = lookup_fields[key]
                parameter_value = key
                if lookup_url_kwarg in kwargs:
                    filter_kwargs[parameter_value] = self.kwargs[lookup_url_kwarg]
        if isinstance(queryset, QuerySet):
            # Ensure queryset is re-evaluated on each request.
            if filter_kwargs is not None:
                queryset = queryset.filter(**filter_kwargs)
            else:
                queryset = queryset.all()

        return queryset

    def check_field_permissions(self, request):
        """
        Review the permissions that the model requests and verify if the user
        who sent the request has them
        """
        permission_per_action = {
            'GET': 'view',
            'POST': 'add',
            'PUT': 'change'
        }

        allowed_field_types = [
            ManyToManyRel, ManyToOneRel, OneToOneRel, ForeignObjectRel
        ]

        # Get the current model
        model = self.get_queryset().model

        # Get the current user
        current_user = request.user

        # obtain the data that will be used as search criteria and thus obtain
        # the model's permissions
        app_label = model._meta.app_label
        model_name = model._meta.model_name
        fields = model._meta.get_fields()

        fields = [field for field in fields if type(field) not in allowed_field_types]

        # List where the permissions per field of the user who sent the
        # request are saved
        user_exclude_fields = list()

        method = request.method
        user_groups = current_user.groups.all()
        permission_in_groups = list()

        # get permissions in group
        for group in user_groups:
            permissions = group.permissions.all()
            for permission in permissions:
                if permission.id not in permission_in_groups:
                    permission_in_groups.append(permission.id)

        # get permissions directly related
        user_permissions = current_user.user_permissions.all().values('id')

        # get all user permissions
        all_user_permissions = Permission.objects.filter(
            Q(id__in=user_permissions) |
            Q(id__in=permission_in_groups),
            content_type__app_label=app_label,
            content_type__model=model_name,
            codename__startswith='{}'.format(permission_per_action.get(method))
        )

        # module general permission (view, add, change, delete)
        model_permission = '{}_{}'.format(
            permission_per_action.get(method),
            model_name
            )

        # if the user does not have the permissions of the model according to
        # the action, all the fields are added to the exclude
        if len(all_user_permissions.filter(codename=model_permission)) == 0:
            user_exclude_fields = [field.name for field in fields]

        # if there are fields in user_exclude_fields it means that the user
        # does not have the general permission of the model
        if len(user_exclude_fields) > 0:
            for field in fields:
                current_permission = all_user_permissions.filter(
                    codename__icontains='{0}__{1}__{2}'.format(
                        permission_per_action.get(method),
                        field.name, model_name
                        )
                    )

                if current_permission.exists():
                    user_exclude_fields.remove(field.name)

        else:
            for field in fields:
                field_permission_exists = Permission.objects.filter(
                    content_type__app_label=app_label,
                    content_type__model=model_name,
                    codename__icontains='{0}__{1}__{2}'.format(
                        permission_per_action.get(method),
                        field.name, model_name
                        )
                )
                if field_permission_exists.exists():
                    current_permission = all_user_permissions.filter(
                        id__in=field_permission_exists.values('id')
                        )
                    if len(current_permission) == 0:
                        user_exclude_fields.append(field.name)

        return user_exclude_fields

    def get_object(self):
        """
        Returns the object the view is displaying.

        You may want to override this if you need to provide non-standard
        queryset lookups.  Eg if objects are referenced using multiple
        keyword arguments in the url conf.
        """
        queryset = self.filter_queryset(self.get_queryset())

        # Perform the lookup filtering.
        lookup_url_kwarg = self.lookup_url_kwarg or self.lookup_field

        assert lookup_url_kwarg in self.kwargs, (
            'Expected view %s to be called with a URL keyword argument '
            'named "%s". Fix your URL conf, or set the `.lookup_field` '
            'attribute on the view correctly.' %
            (self.__class__.__name__, lookup_url_kwarg)
        )

        filter_kwargs = {self.lookup_field: self.kwargs[lookup_url_kwarg]}
        obj = get_object_or_404(queryset, **filter_kwargs)

        # May raise a permission denied
        self.check_object_permissions(self.request, obj)

        return obj

    def get_serializer(self, *args, **kwargs):
        """
        Return the serializer instance that should be used for validating and
        deserializing input, and for serializing output.
        """
        serializer_class = self.get_serializer_class()
        kwargs['context'] = self.get_serializer_context()
        if isinstance(self.serializer_class, DynamicFieldsModelSerializer):
            return serializer_class(remove_fields=self.fields, *args, **kwargs)
        return serializer_class(*args, **kwargs)

    def get_serializer_list(self, *args, **kwargs):
        """
        Return the serializer instance that should be used for validating and
        deserializing input, and for serializing output.
        """
        serializer_class = self.get_serializer_class_list()
        kwargs['context'] = self.get_serializer_context()
        if isinstance(self.serializer_list, DynamicFieldsModelSerializer):
            return serializer_class(remove_fields=self.fields, *args, **kwargs)
        return serializer_class(*args, **kwargs)

    def get_serializer_class_list(self):
        """
        Return the class to use for the serializer.
        Defaults to using `self.serializer_class`.

        You may want to override this if you need to provide different
        serializations depending on the incoming request.

        (Eg. admins get full serialization, others get basic serialization)
        """
        if self.serializer_list is not None:
            return self.serializer_list

        return self.get_serializer_class()

    def get_response_serializer(self, *args, **kwargs):
        """
        Return the serializer instance that should be used for validating and
        deserializing input, and for serializing output.
        """
        serializer_class = self.get_serializer_class_response()
        kwargs['context'] = self.get_serializer_context()
        if isinstance(self.serializer_list, DynamicFieldsModelSerializer):
            return serializer_class(remove_fields=self.fields, *args, **kwargs)
        return serializer_class(*args, **kwargs)

    def get_serializer_class_response(self):
        """
        Return the class to use for the serializer.
        Defaults to using `self.serializer_class`.

        You may want to override this if you need to provide different
        serializations depending on the incoming request.

        (Eg. admins get full serialization, others get basic serialization)
        """
        if self.response_serializer is not None:
            return self.response_serializer

        return self.get_serializer_class()


class SoftDestroyAPIView(SoftDestroyModelMixin, CustomGenericAPIView):
    """
    Concrete view for inactive registers without deleting
    """

    @transaction.atomic
    def delete(self, request, *args, **kwargs):
        return self.destroy(request, *args, **kwargs)


class RetrieveUpdateDestroyAPIView(RetrieveModelMixin,
                                   UpdateModelMixin,
                                   SoftDestroyModelMixin,
                                   CustomGenericAPIView):
    """
    Concrete view for retrieving, updating or deleting a model instance.
    """

    def get(self, request, *args, **kwargs):
        return self.retrieve(request, *args, **kwargs)

    @transaction.atomic
    def put(self, request, *args, **kwargs):
        self.fields = self.check_field_permissions(request)
        return self.update(request, *args, **kwargs)

    @transaction.atomic
    def patch(self, request, *args, **kwargs):
        return self.partial_update(request, *args, **kwargs)

    @transaction.atomic
    def delete(self, request, *args, **kwargs):
        return self.destroy(request, *args, **kwargs)


class RetrieveUpdateModelDestroyAPIView(RetrieveModelMixin,
                                        UpdateModelMixin,
                                        ModelDestroyModelMixin,
                                        CustomGenericAPIView):
    """
    Concrete view for retrieving, updating or deleting a model instance.
    """

    def get(self, request, *args, **kwargs):
        return self.retrieve(request, *args, **kwargs)

    @transaction.atomic
    def put(self, request, *args, **kwargs):
        self.fields = self.check_field_permissions(request)
        return self.update(request, *args, **kwargs)

    @transaction.atomic
    def patch(self, request, *args, **kwargs):
        return self.partial_update(request, *args, **kwargs)

    @transaction.atomic
    def delete(self, request, *args, **kwargs):
        return self.destroy(request, *args, **kwargs)


class ListCreateAPIView(ListModelMixin,
                        CreateModelMixin,
                        CustomGenericAPIView):
    """
    Concrete view for listing a queryset or creating a model instance.
    """

    def get(self, request, *args, **kwargs):
        self.fields = self.check_field_permissions(request)
        return self.list(request, *args, **kwargs)

    @transaction.atomic
    def post(self, request, *args, **kwargs):
        self.fields = self.check_field_permissions(request)
        return self.create(request, *args, **kwargs)


class ListAPIView(ListModelMixin,
                  CustomGenericAPIView):
    """
    Concrete view for listing a queryset.
    """

    def get(self, request, *args, **kwargs):
        self.fields = self.check_field_permissions(request)
        return self.list(request, *args, **kwargs)


class RetrieveAPIView(RetrieveModelMixin,
                      CustomGenericAPIView):
    """
    Concrete view for retrieving model instance.
    """
    lookup_field = 'slug'

    def get(self, request, *args, **kwargs):
        return self.retrieve(request, *args, **kwargs)


class RetrieveDestroyAPIView(RetrieveModelMixin,
                             SoftDestroyModelMixin,
                             CustomGenericAPIView):
    """
    Concrete view for retrieving, updating or deleting a model instance.
    """

    def get(self, request, *args, **kwargs):
        return self.retrieve(request, *args, **kwargs)

    @transaction.atomic
    def delete(self, request, *args, **kwargs):
        return self.destroy(request, *args, **kwargs)


class UpdateAPIView(UpdateModelMixin, CustomGenericAPIView):
    """
    Concrete view for retrieving, updating or deleting a model instance.
    """

    @transaction.atomic
    def put(self, request, *args, **kwargs):
        self.fields = self.check_field_permissions(request)
        return self.update(request, *args, **kwargs)

    @transaction.atomic
    def patch(self, request, *args, **kwargs):
        self.fields = self.check_field_permissions(request)
        return self.partial_update(request, *args, **kwargs)


class RetrieveUpdateAPIView(RetrieveModelMixin,
                            UpdateModelMixin,
                            CustomGenericAPIView):
    """
    Concrete view for retrieving, updating or deleting a model instance.
    """

    def get(self, request, *args, **kwargs):
        return self.retrieve(request, *args, **kwargs)

    @transaction.atomic
    def put(self, request, *args, **kwargs):
        self.fields = self.check_field_permissions(request)
        return self.update(request, *args, **kwargs)

    @transaction.atomic
    def patch(self, request, *args, **kwargs):
        self.fields = self.check_field_permissions(request)
        return self.partial_update(request, *args, **kwargs)


class CreateAPIView(CreateModelMixin, CustomGenericAPIView):
    """
    Concrete view for creating a model.
    """

    @transaction.atomic
    def post(self, request, *args, **kwargs):
        self.fields = self.check_field_permissions(request)
        return self.create(request, *args, **kwargs)
