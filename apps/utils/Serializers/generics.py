#!/usr/bin/env python
# -*- coding: utf-8 -*-

# logger
import logging

# Django REST Framework
from rest_framework.serializers import ModelSerializer

VERSION = (0, 0, 2)
__author__ = 'Alfredo Aguilar'
__mail__ = 'aguilarhernandeza@gmail.com'
__date__ = '12/02/2020'
__modified__ = '12/02/2020'
__version__ = '.'.join([str(x) for x in VERSION])

ALL_FIELDS = '__all__'

logger = logging.getLogger('server')


class DynamicFieldsModelSerializer(ModelSerializer):
    """
    A ModelSerializer that takes an additional `fields` argument that
    controls which fields should be displayed.
    """

    def __init__(self, *args, **kwargs):
        # Don't pass the 'fields' arg up to the superclass
        remove_fields = kwargs.pop('remove_fields', None)

        # Instantiate the superclass normally
        super(DynamicFieldsModelSerializer, self).__init__(*args, **kwargs)
        if remove_fields is not None:
            for field_name in remove_fields:
                try:
                    self.fields.pop(field_name)
                except Exception as e:
                    logger.info('Serializador dinamico: Error {}'.format(e))
