#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Serializer
from .generics import GenericSerializer


VERSION = (0, 0, 2)
__author__ = 'Alfredo Aguilar'
__mail__ = 'aguilarhernandeza@gmail.com'
__date__ = '12/02/2020'
__modified__ = '12/02/2020'
__version__ = '.'.join([str(x) for x in VERSION])


def get_serializer_class(model, fields=None, exclude=None):
    """
        Return a serialized data with custom model, fields and exclude fields
    """

    if fields is None:
        fields = '__all__'

    GenericSerializer.Meta.model = model
    GenericSerializer.Meta.fields = fields
    GenericSerializer.Meta.exclude = exclude

    return GenericSerializer


def save_object_serializer(custom_serializer, data):
    """ Generic save serializer """
    serializer = custom_serializer(data=data)
    serializer.is_valid(raise_exception=True)
    serializer.save()
    return serializer
